package com.haliksar.devel.core.di

import com.haliksar.devel.core.Core
import org.koin.dsl.module

val CoreModules = module {
    single { Core() }
}