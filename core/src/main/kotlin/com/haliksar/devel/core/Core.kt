package com.haliksar.devel.core

class Core {
    fun sayHello() = println("Ohh, ok... Hello ${this::class.simpleName}")
}