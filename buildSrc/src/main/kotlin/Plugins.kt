object Plugins {
    const val kotlin = "kotlin"
    const val kotlinKapt = "kotlin-kapt"
    const val koin = "koin"
}