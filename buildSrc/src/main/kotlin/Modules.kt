object Modules {
    const val app = ":app"
    const val moduleOne = ":moduleone"
    const val moduleTwo = ":moduletwo"
    const val core = ":core"
}