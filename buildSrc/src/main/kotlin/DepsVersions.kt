object DepsVersions {
    const val kotlin = "1.3.72"
    const val koin = "2.1.6"
    const val kotest = "4.1.3"
    const val mockk = "1.10.0"
    const val coroutines = "1.3.8"
}