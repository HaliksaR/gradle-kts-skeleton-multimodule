plugins { application }

application {
    mainClassName = "com.haliksar.devel.app.ApplicationKt"
}

dependencies {
    implementation(project(Modules.moduleOne))
    implementation(project(Modules.moduleTwo))
    implementation(project(Modules.core))
}