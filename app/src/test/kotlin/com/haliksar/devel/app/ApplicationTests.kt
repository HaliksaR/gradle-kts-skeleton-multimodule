package com.haliksar.devel.app

import com.haliksar.devel.app.di.ApplicationModules
import com.haliksar.devel.core.di.CoreModules
import com.haliksar.devel.moduleone.ModuleOne
import com.haliksar.devel.moduleone.di.ModuleOneModules
import com.haliksar.devel.moduletwo.di.ModuleTwoModules
import io.kotest.core.listeners.TestListener
import io.kotest.core.spec.style.StringSpec
import io.kotest.koin.KoinListener
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe
import org.koin.test.KoinTest
import org.koin.test.inject


class ApplicationTests : StringSpec(), KoinTest {

    private val modules = listOf(
        CoreModules,
        ModuleOneModules,
        ModuleTwoModules,
        ApplicationModules
    )

    override fun listeners(): List<TestListener> = listOf(KoinListener(modules))

    init {
        val app: Application by inject()

        "Say Hello" {
            app.callModule1() shouldBe "Hello ${ModuleOne::class.simpleName}"
        }

        "Say Hello Error"{
            app.callModule1() shouldNotBe "Hellfo ${ModuleOne::class.simpleName}"
        }
    }
}