package com.haliksar.devel.app.di

import com.haliksar.devel.app.Application
import org.koin.dsl.module

val ApplicationModules = module {
    single {
        Application(
            module1 = get(),
            module2 = get()
        )
    }
}