package com.haliksar.devel.app

import com.haliksar.devel.app.di.ApplicationModules
import com.haliksar.devel.core.di.CoreModules
import com.haliksar.devel.moduleone.ModuleOne
import com.haliksar.devel.moduleone.di.ModuleOneModules
import com.haliksar.devel.moduletwo.ModuleTwo
import com.haliksar.devel.moduletwo.di.ModuleTwoModules
import org.koin.core.KoinComponent
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.core.get

fun main(args: Array<String>) {
    Application.start()
        .apply {
            callModule1()
            callCoreModule1()

            callModule2()
            callCoreModule2()
        }
    Application.stop()
}


class Application(
    private val module1: ModuleOne,
    private val module2: ModuleTwo
) : KoinComponent {

    companion object : KoinComponent {

        private fun initKoin() {
            startKoin {
                modules(
                    CoreModules,
                    ModuleOneModules,
                    ModuleTwoModules,
                    ApplicationModules
                )
            }
        }

        fun start(): Application {
            initKoin()
            return get()
        }

        fun stop() {
            stopKoin()
        }
    }

    fun callModule1() = module1.sayHello()
    fun callCoreModule1() = module1.callCore()

    fun callModule2() = module2.sayHello()
    fun callCoreModule2() = module2.callCore()
}