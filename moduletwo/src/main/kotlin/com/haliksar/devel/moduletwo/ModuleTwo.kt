package com.haliksar.devel.moduletwo

import com.haliksar.devel.core.Core
import org.koin.core.KoinComponent
import org.koin.core.inject

class ModuleTwo: KoinComponent {

    private val core by inject<Core>()

    fun sayHello() = "Hello ${this::class.simpleName}"

    fun callCore() {
        println("Hey, core! Say Hello!")
        core.sayHello()
    }
}