package com.haliksar.devel.moduletwo.di

import com.haliksar.devel.moduletwo.ModuleTwo
import org.koin.dsl.module

val ModuleTwoModules = module {
    single { ModuleTwo() }
}