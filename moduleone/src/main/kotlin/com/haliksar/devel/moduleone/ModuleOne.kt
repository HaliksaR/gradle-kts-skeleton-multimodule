package com.haliksar.devel.moduleone

import com.haliksar.devel.core.Core
import org.koin.core.KoinComponent
import org.koin.core.inject

class ModuleOne : KoinComponent {

    private val core by inject<Core>()

    fun sayHello() = "Hello ${this::class.simpleName}"

    fun callCore() {
        println("Hey, core! Say Hello!")
        core.sayHello()
    }
}