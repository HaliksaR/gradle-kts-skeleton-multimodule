package com.haliksar.devel.moduleone.di

import com.haliksar.devel.moduleone.ModuleOne
import org.koin.dsl.module

val ModuleOneModules = module {
    single { ModuleOne() }
}