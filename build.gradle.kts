import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val compileKotlin: KotlinCompile by tasks
val compileTestKotlin: KotlinCompile by tasks

plugins {
    kotlin("jvm") version DepsVersions.kotlin
    kotlin("kapt") version DepsVersions.kotlin
}

group = AppInfo.group
version = AppInfo.version

repositories {
    mavenCentral()
    jcenter()
}

buildscript {
    repositories {
        mavenCentral()
        jcenter()
    }

    dependencies {
        classpath(kotlin("gradle-plugin", version = DepsVersions.kotlin))
        classpath(koin("gradle-plugin", version = DepsVersions.koin))
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}

subprojects {
    apply(plugin = Plugins.kotlin)
    apply(plugin = Plugins.kotlinKapt)
    apply(plugin = Plugins.koin)

    repositories {
        mavenCentral()
        jcenter()
    }

    dependencies {
        implementation(kotlin("stdlib-jdk8", version = DepsVersions.kotlin))
        implementation(kotlin("compiler-embeddable", version = DepsVersions.kotlin))

        implementation(kotlinx("coroutines-core", version = DepsVersions.coroutines))

        implementation(koin("core", version = DepsVersions.koin))
        implementation(koin("core-ext", version = DepsVersions.koin))
        testImplementation(koin("test", version = DepsVersions.koin))

        testImplementation(kotest("runner-junit5-jvm", version = DepsVersions.kotest))
        testImplementation(kotest("assertions-core-jvm", version = DepsVersions.kotest))
        testImplementation(kotest("property-jvm", version = DepsVersions.kotest))
        testImplementation(kotest("extensions-koin-jvm", version = DepsVersions.kotest))

        testImplementation(mockk(version = DepsVersions.mockk))
    }

    compileKotlin.kotlinOptions.jvmTarget = OtherVersions.jvm
    compileTestKotlin.kotlinOptions.jvmTarget = OtherVersions.jvm
}